package grails3vaadin8springboot

import com.alsnightsoft.vaadin8.demo.domains.User
import com.alsnightsoft.vaadin8.demo.services.UserService
import com.alsnightsoft.vaadin8.demo.utils.Grails

class BootStrap {

    def init = { servletContext ->
        for (int i = 1; i <= 5; i++) {
            createUser("test" + i, "test"+ i, "testing"+ i, "user"+ i)
        }
    }

    private static void createUser(String username, String password, String firstnames, String lastnames) {
        User user = Grails.get(UserService).byUsername(username)
        if (!user) {
            user = new User()
            user.username = username
            user.password = password.encodeAsSHA256()
            user.firstnames = firstnames
            user.lastnames = lastnames
            Grails.get(UserService).bootStrap(user)
        }
    }

    def destroy = {
    }
}
