package com.alsnightsoft.vaadin8.demo

import groovy.transform.CompileStatic

import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

/**
 *  Created by aluis on 1/16/17.
 */
@CompileStatic
@Path('/api/demo')
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.TEXT_PLAIN)
class TestResource {

    @GET
    static String isServiceUp() {
        return "OK"
    }
}
