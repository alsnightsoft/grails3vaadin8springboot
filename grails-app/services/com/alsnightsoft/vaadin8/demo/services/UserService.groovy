package com.alsnightsoft.vaadin8.demo.services

import com.alsnightsoft.vaadin8.demo.domains.User
import grails.transaction.Transactional

@Transactional
class UserService {

    private User createBase(User user) {
        if (user != null && !user.hasErrors()) {
            if (User.exists(user.id)) {
                return user.merge(flush: true, failOnError: true)
            } else {
                return user.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    User bootStrap(User user) {
        return createBase(user)
    }

    User create(User user) {
        return createBase(user)
    }

    List<User> list(boolean enabled, int start, int size) {
        return User.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return User.countByEnabled(enabled)
    }

    User byUsername(String username) {
        return User.findByUsername(username)
    }

    boolean delete(User user) {
        if (!user) {
            return false
        }
        user = User.findById(user.id)
        user.setEnabled(!user.getEnabled())
        return create(user)
    }
}
