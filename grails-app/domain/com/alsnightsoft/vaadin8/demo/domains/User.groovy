package com.alsnightsoft.vaadin8.demo.domains

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class User {

    String username
    String password // sha256
    String firstnames
    String lastnames

    boolean enabled = true

    static constraints = {
        username unique: true

        firstnames nullable: true
        lastnames nullable: true
    }

    static mapping = {
        table "users"
    }
}
