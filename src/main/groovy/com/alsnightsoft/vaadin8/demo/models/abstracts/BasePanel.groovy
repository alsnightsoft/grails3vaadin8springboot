package com.alsnightsoft.vaadin8.demo.models.abstracts

import com.vaadin.ui.Panel
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
abstract class BasePanel extends Panel {

    protected abstract void build()
}
