package com.alsnightsoft.vaadin8.demo.models

import com.vaadin.data.provider.DataProvider
import com.vaadin.ui.Grid
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class GridManager<T> {

    private DataProvider dataProvider
    private Grid<T> grid

    GridManager(Grid<T> grid, DataProvider dataProvider) {
        this.grid = grid
        this.dataProvider = dataProvider
        configureGrid()
    }

    private void configureGrid() {
        grid.setDataProvider(dataProvider)
    }

    final void clearSelect() {
        grid.deselectAll()
    }

    final boolean haveSelection() {
        return grid.getSelectedItems().size() >= 1
    }

    final void updateTable() {
        grid.clearSortOrder()
        clearSelect()
    }

    final Grid<T> getGrid() {
        return grid
    }
}
