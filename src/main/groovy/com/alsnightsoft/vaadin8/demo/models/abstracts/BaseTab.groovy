package com.alsnightsoft.vaadin8.demo.models.abstracts

import com.alsnightsoft.vaadin8.demo.models.GridManager
import com.alsnightsoft.vaadin8.demo.ui.components.Menu
import com.alsnightsoft.vaadin8.demo.utils.Grails
import com.vaadin.data.HasValue
import com.vaadin.data.provider.DataProvider
import com.vaadin.ui.Grid
import com.vaadin.ui.VerticalLayout
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
abstract class BaseTab<T> extends BasePanel {

    protected VerticalLayout mainLayout = new VerticalLayout()

    protected Menu menu = new Menu()
    private Grid<T> grid = new Grid<>()
    public GridManager<T> gridManager

    protected BaseTab() {
        mainLayout.setSizeFull()
        mainLayout.setSpacing(true)

        grid.setSizeFull()

        mainLayout.addComponent(menu)
        mainLayout.addComponent(grid)

        setContent(mainLayout)

        menu.chkShowDelete.addValueChangeListener(new HasValue.ValueChangeListener<Boolean>() {
            @Override
            void valueChange(HasValue.ValueChangeEvent<Boolean> event) {
                if (menu.chkShowDelete.getValue()) {
                    menu.btnDelete.setCaption(Grails.i18n("action.restore"))
                } else {
                    menu.btnDelete.setCaption(Grails.i18n("action.delete"))
                }
                updateTable()
            }
        })

        build()
    }

    protected void manageGrid(DataProvider dataProvider) {
        gridManager = new GridManager(grid, dataProvider)
    }

    boolean showDelete() {
        return !menu.chkShowDelete.getValue()
    }

    final void updateTable() {
        if (gridManager) {
            gridManager.updateTable()
        }
    }
}
