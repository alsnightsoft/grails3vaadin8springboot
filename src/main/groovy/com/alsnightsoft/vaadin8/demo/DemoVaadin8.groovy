package com.alsnightsoft.vaadin8.demo

import com.alsnightsoft.vaadin8.demo.ui.UserTab
import com.alsnightsoft.vaadin8.demo.ui.components.AdmMenu
import com.alsnightsoft.vaadin8.demo.utils.Themes
import com.alsnightsoft.vaadin8.demo.utils.configurations.VaadinConfiguration
import com.vaadin.annotations.Push
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Widgetset
import com.vaadin.server.VaadinRequest
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.ui.UI
import com.vaadin.ui.VerticalLayout
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 2/26/17.
 */
@Push
@Theme(Themes.DEMO)
@Widgetset("AppWidgetSet")
@SpringUI(path = VaadinConfiguration.APP)
@CompileStatic
class DemoVaadin8 extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout mainLayout = new VerticalLayout()
        mainLayout.addComponent(new AdmMenu())
        mainLayout.addComponent(new UserTab())
        setContent(mainLayout)
    }
}
