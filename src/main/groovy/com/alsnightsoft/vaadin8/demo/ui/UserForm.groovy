package com.alsnightsoft.vaadin8.demo.ui

import com.alsnightsoft.vaadin8.demo.domains.User
import com.alsnightsoft.vaadin8.demo.services.UserService
import com.alsnightsoft.vaadin8.demo.utils.Grails
import com.vaadin.data.*
import com.vaadin.server.Setter
import com.vaadin.server.Sizeable
import com.vaadin.ui.*
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 4/9/17.
 */
@CompileStatic
class UserForm extends Window {

    private VerticalLayout mainLayout = new VerticalLayout()

    private Binder<User> binder = new Binder<>()

    private TextField tfUsername
    private TextField tfPassword
    private TextField tfNames
    private TextField tfLastnames

    private Button btnOK
    private Button btnCancel
    private Button btnReset

    User userSelect

    UserForm() {
        this(null)
    }

    UserForm(User userSelect) {
        this.userSelect = userSelect
        buildLayout()
        setContent(mainLayout)
    }

    private void buildLayout() {
        center()
        setWidth(800, Sizeable.Unit.PIXELS)
        setHeight(600, Sizeable.Unit.PIXELS)

        tfUsername = new TextField("UserName")
        tfPassword = new TextField("Password")
        tfNames = new TextField("Names")
        tfLastnames = new TextField("Lastnames")

        binder.forField(tfUsername).asRequired("Username requerido").bind(new ValueProvider<User, String>() {
            @Override
            String apply(User user) {
                return user.username
            }
        }, new Setter<User, String>() {
            @Override
            void accept(User user, String s) {
                user.username = s
            }
        })

        binder.forField(tfPassword).asRequired("Password requerido").bind(new ValueProvider<User, String>() {
            @Override
            String apply(User user) {
                return user.password
            }
        }, new Setter<User, String>() {
            @Override
            void accept(User user, String s) {
                user.password = s
            }
        })

        binder.forField(tfNames).bind(new ValueProvider<User, String>() {
            @Override
            String apply(User user) {
                return user.firstnames
            }
        }, new Setter<User, String>() {
            @Override
            void accept(User user, String s) {
                user.firstnames = s
            }
        })

        binder.forField(tfLastnames).bind(new ValueProvider<User, String>() {
            @Override
            String apply(User user) {
                return user.lastnames
            }
        }, new Setter<User, String>() {
            @Override
            void accept(User user, String s) {
                user.lastnames = s
            }
        })

        if (userSelect) {
            binder.readBean(userSelect)
        }

        btnOK = new Button("OK")
        btnOK.setEnabled(false)
        btnOK.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (binder.writeBeanIfValid(userSelect)) {
                    Grails.get(UserService).bootStrap(userSelect)
                } else {
                    for (BindingValidationStatus bindingValidationStatus : binder.validate().fieldValidationErrors) {
                        if (bindingValidationStatus.field instanceof Component.Focusable) {
                            (bindingValidationStatus.field as Component.Focusable).focus()
                        }
                        break
                    }
                }
            }
        })
        btnCancel = new Button("Cancel")
        btnCancel.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                close()
            }
        })

        btnReset = new Button("Rest")
        btnReset.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (userSelect) {
                    binder.readBean(userSelect)
                }
            }
        })

        binder.addStatusChangeListener(new StatusChangeListener() {
            @Override
            void statusChange(StatusChangeEvent event) {
                btnOK.setEnabled(event.getBinder().hasChanges() && event.getBinder().isValid())
            }
        })

        mainLayout.addComponent(tfUsername)
        mainLayout.addComponent(tfPassword)
        mainLayout.addComponent(tfNames)
        mainLayout.addComponent(tfLastnames)
        mainLayout.addComponent(btnOK)
        mainLayout.addComponent(btnCancel)
        mainLayout.addComponent(btnReset)
    }
}
