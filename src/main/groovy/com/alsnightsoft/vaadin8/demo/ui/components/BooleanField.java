package com.alsnightsoft.vaadin8.demo.ui.components;

import com.vaadin.ui.*;

/**
 * Demo Custom field
 * <p>
 * Created by aluis on 11/23/16.
 */
public class BooleanField extends CustomField<Boolean> {

    private Button button;

    public BooleanField() {
        button = new Button();
        doSetValue(false);
    }

    @Override
    protected Component initContent() {
        button.addClickListener((Button.ClickListener) event -> doSetValue(!getValue()));
        return new VerticalLayout(new Label("Click the button"), button);
    }

    @Override
    public Boolean getValue() {
        return button.getCaption().equals("On");
    }

    @Override
    protected void doSetValue(Boolean value) {
        button.setCaption(value ? "On" : "Off");
    }
}