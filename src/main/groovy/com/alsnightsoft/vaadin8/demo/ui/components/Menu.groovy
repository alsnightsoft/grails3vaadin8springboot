package com.alsnightsoft.vaadin8.demo.ui.components

import com.alsnightsoft.vaadin8.demo.utils.Grails
import com.alsnightsoft.vaadin8.demo.models.abstracts.BasePanel
import com.vaadin.server.Sizeable
import com.vaadin.ui.Alignment
import com.vaadin.ui.Button
import com.vaadin.ui.CheckBox
import com.vaadin.ui.HorizontalLayout
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class Menu extends BasePanel {

    private HorizontalLayout mainLayout = new HorizontalLayout()

    private Button btnNew
    private Button btnEdit
    private Button btnDelete
    private CheckBox chkShowDelete

    public Menu() {
        build()
    }

    @Override
    protected void build() {
        mainLayout.setWidth(100, Sizeable.Unit.PERCENTAGE)
        mainLayout.setSpacing(true)

        btnNew = new Button(Grails.i18n("action.new"))
        btnNew.setWidth(100, Sizeable.Unit.PERCENTAGE)

        btnEdit = new Button(Grails.i18n("action.edit"))
        btnEdit.setWidth(100, Sizeable.Unit.PERCENTAGE)

        btnDelete = new Button(Grails.i18n("action.delete"))
        btnDelete.setWidth(100, Sizeable.Unit.PERCENTAGE)

        chkShowDelete = new CheckBox(Grails.i18n("action.show.delete"))
        chkShowDelete.setWidth(100, Sizeable.Unit.PERCENTAGE)

        mainLayout.addComponent(btnNew)
        mainLayout.addComponent(btnEdit)
        mainLayout.addComponent(btnDelete)
        mainLayout.addComponent(chkShowDelete)
        mainLayout.setComponentAlignment(chkShowDelete, Alignment.MIDDLE_CENTER)

        setContent(mainLayout)
    }

    public Button getBtnNew() {
        return btnNew
    }

    public Button getBtnEdit() {
        return btnEdit
    }

    public Button getBtnDelete() {
        return btnDelete
    }

    public CheckBox getChkShowDelete() {
        return chkShowDelete
    }
}
