package com.alsnightsoft.vaadin8.demo.ui.components

import com.alsnightsoft.vaadin8.demo.utils.Grails
import com.alsnightsoft.vaadin8.demo.models.abstracts.BasePanel
import com.vaadin.server.Sizeable
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Button
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.Label
import grails.util.Holders
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/23/16.
 */
@CompileStatic
class AdmMenu extends BasePanel {

    public static final Locale SPANISH = new Locale("es")

    private HorizontalLayout mainLayout = new HorizontalLayout()

    private Button btnLanguage
    private BooleanField booleanField

    public AdmMenu() {
        build()
    }

    @Override
    protected void build() {
        mainLayout.setWidth(100, Sizeable.Unit.PERCENTAGE)
        mainLayout.setSpacing(true)

        btnLanguage = new Button(Grails.i18n("action.language"))
        btnLanguage.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (VaadinSession.getCurrent().locale.language == Locale.ENGLISH.language) {
                    VaadinSession.getCurrent().locale = SPANISH
                } else {
                    VaadinSession.getCurrent().locale = Locale.ENGLISH
                }
                getUI().getPage().reload()
            }
        })
        booleanField = new BooleanField()

        mainLayout.addComponent(btnLanguage)
        mainLayout.addComponent(new Label(Grails.i18n("action.version") + " : " + Holders.grailsApplication.metadata['info.app.version']))
        mainLayout.addComponent(booleanField)

        setContent(mainLayout)
    }
}
