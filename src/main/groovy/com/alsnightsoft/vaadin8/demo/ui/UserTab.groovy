package com.alsnightsoft.vaadin8.demo.ui

import com.alsnightsoft.vaadin8.demo.domains.User
import com.alsnightsoft.vaadin8.demo.models.abstracts.BaseTab
import com.alsnightsoft.vaadin8.demo.services.UserService
import com.alsnightsoft.vaadin8.demo.utils.Grails
import com.vaadin.data.ValueProvider
import com.vaadin.data.provider.AbstractBackEndDataProvider
import com.vaadin.data.provider.Query
import com.vaadin.event.selection.SelectionEvent
import com.vaadin.event.selection.SelectionListener
import com.vaadin.ui.Button
import com.vaadin.ui.Notification
import groovy.transform.CompileStatic

import java.util.stream.Stream

/**
 * Tab de usuario.
 * <p>
 * Created by aluis on 11/22/16.
 */
@CompileStatic
class UserTab extends BaseTab<User> {

    private User userSelect

    UserTab() {
    }

    @Override
    protected void build() {
        manageGrid(new AbstractBackEndDataProvider<User, String>() {
            @Override
            protected Stream<User> fetchFromBackEnd(Query<User, String> query) {
                return Grails.get(UserService).list(showDelete(), query.getOffset(), query.getLimit()).stream()
            }

            @Override
            protected int sizeInBackEnd(Query<User, String> query) {
                return Grails.get(UserService).count(showDelete())
            }

            @Override
            boolean isInMemory() {
                return false
            }
        })

        gridManager.getGrid().addColumn(new ValueProvider<User, Long>() {
            @Override
            Long apply(User user) {
                return user.id
            }
        }).setCaption("ID")
        gridManager.getGrid().addColumn(new ValueProvider<User, String>() {
            @Override
            String apply(User user) {
                return user.username
            }
        }).setCaption("Username")
        gridManager.getGrid().addColumn(new ValueProvider<User, String>() {
            @Override
            String apply(User user) {
                return user.firstnames
            }
        }).setCaption("Firstnames")
        gridManager.getGrid().addColumn(new ValueProvider<User, String>() {
            @Override
            String apply(User user) {
                return user.lastnames
            }
        }).setCaption("Lastnames")
        gridManager.getGrid().addColumn(new ValueProvider<User, Boolean>() {
            @Override
            Boolean apply(User user) {
                return user.enabled
            }
        }).setCaption("Enable")

        gridManager.getGrid().addSelectionListener(new SelectionListener<User>() {
            @Override
            void selectionChange(SelectionEvent<User> event) {
                if (gridManager.haveSelection()) {
                    for (User user : gridManager.getGrid().getSelectedItems()) {
                        userSelect = user
                    }
                } else {
                    userSelect = null
                }
            }
        })
        // testing! :V
        menu.getBtnNew().addClickListener({ Button.ClickEvent event ->
            Notification.show(Grails.i18n("notification.new"), Grails.i18n("notification.new.description"), Notification.Type.TRAY_NOTIFICATION)
            UserForm userForm = new UserForm()
            getUI().addWindow(userForm)
        })
        menu.getBtnEdit().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                Notification.show(Grails.i18n("notification.edit"), Grails.i18n("notification.edit.description"), Notification.Type.TRAY_NOTIFICATION)
                UserForm userForm = new UserForm(userSelect)
                getUI().addWindow(userForm)
            }
        })
        menu.getBtnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (userSelect != null) {
                    Grails.get(UserService.class).delete(userSelect)
                    updateTable()
                }
            }
        })
    }
}
