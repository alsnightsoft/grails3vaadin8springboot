package com.alsnightsoft.vaadin8.demo.utils

import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class Themes {

    public static final String DEMO = "Grails3Vaadin8SpringBoot"
}
