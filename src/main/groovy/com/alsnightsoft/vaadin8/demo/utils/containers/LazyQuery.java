package com.alsnightsoft.vaadin8.demo.utils.containers;

import java.util.List;

/**
 * Execute query on grails service
 * <p>
 * Created by aluis on 11/22/16.
 */
public interface LazyQuery<T> {

    /**
     * Get Size by a Query
     */
    int getSize();

    /**
     * Get items to view on the page using the same model of the container.
     */
    List<T> getItemsIds(int startIndex, int numberOfIds);

    /**
     * Get Size for manual filter.
     */
    int getFilteredSize();

    /**
     * Get items to view on the page for manual filter using the same model of the container.
     */
    List<T> getFilteredItemsIds(int startIndex, int numberOfIds);
}
