package com.alsnightsoft.vaadin8.demo.utils.configurations

import com.vaadin.spring.annotation.EnableVaadin
import com.vaadin.spring.server.SpringVaadinServlet
import groovy.transform.CompileStatic
import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 *  Created by aluis on 7/4/16.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Configuration
@EnableVaadin
@CompileStatic
class VaadinConfiguration {

    static final String APP = "/"

    @Bean
    ServletRegistrationBean servletRegistrationBean() {
        SpringVaadinServlet springVaadinServlet = new SpringVaadinServlet()
        springVaadinServlet.serviceUrlPath = APP
        return new ServletRegistrationBean(springVaadinServlet,  APP + "*", "/VAADIN/*")
    }
}
